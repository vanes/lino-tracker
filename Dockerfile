FROM ubuntu:latest
RUN apt-get update
RUN apt-get install libgmp-dev -y
RUN apt-get install build-essential flex bison -y


WORKDIR /home
COPY . /home

RUN apt-get install  golang-go -y

RUN cp -r vendor/* /usr/lib/go-1.10/src
RUN mkdir /usr/lib/go-1.10/src/github.com/lino-network/lino-go
RUN mkdir /usr/lib/go-1.10/src/github.com/lino-network/lino-go/api
RUN mkdir /usr/lib/go-1.10/src/github.com/lino-network/lino-go/broadcast
RUN mkdir /usr/lib/go-1.10/src/github.com/lino-network/lino-go/query
RUN mkdir /usr/lib/go-1.10/src/github.com/lino-network/lino-go/transport
RUN mkdir /usr/lib/go-1.10/src/github.com/lino-network/lino-go/errors
RUN mkdir /usr/lib/go-1.10/src/github.com/lino-network/lino-go/model

RUN cp -r api/* /usr/lib/go-1.10/src/github.com/lino-network/lino-go/api
RUN cp -r broadcast/* /usr/lib/go-1.10/src/github.com/lino-network/lino-go/broadcast
RUN cp -r query/* /usr/lib/go-1.10/src/github.com/lino-network/lino-go/query
RUN cp -r transport/* /usr/lib/go-1.10/src/github.com/lino-network/lino-go/transport
RUN cp -r errors/* /usr/lib/go-1.10/src/github.com/lino-network/lino-go/errors
RUN cp -r model/* /usr/lib/go-1.10/src/github.com/lino-network/lino-go/model

RUN GOPATH=/home/example
RUN PATH=$GOPATH/bin:$PATH
RUN export GOROOT GOPATH PATH
RUN cd example && go build web.go
RUN ln example/web /usr/bin/web
EXPOSE 8080
CMD ["/usr/bin/web"]


