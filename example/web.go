package main

import (
	//"encoding/hex"
	"fmt"
    "encoding/json"
    "time"
	"github.com/lino-network/lino-go/api"
	//"github.com/lino-network/lino-go/model"
	//"github.com/lino-network/lino-go/transport"
	//"github.com/tendermint/tendermint/crypto/secp256k1"
	"html/template"
    "net/http"
    "path"
    "strconv"
)


var username string = "lino"

type ViewData struct{
    Username string
    Amount [12]int64
    Saving_amount int64
	Coin_day_amount int64
	Num_of_tr int
	Num_of_rw int
	Total_income int64
	Original_income int64
	Friction_income int64
	Inflation_income int64 
	Unclaim_reward int64
	ValidatorsList []string
	Val_deposit []string
	Val_absent_commit []int
	Val_produced_blocks []int
	Val_address []string
	LatestBlockTime [10]string
	LatestBlockNum_txs [10]int64
	LatestBlockChain_id [10]string
	LatestBlockTot_txs [10]int64
	LatestBlockHeight [10]int64
}

type ParamData struct{
    Username string
    Consumption_time_adjust_base int 
	Consumption_time_adjust_offset int 
	Num_of_consumption_on_author_offset int 
	Total_amount_of_consumption_base int 
	Total_amount_of_consumption_offset int 
	Amount_of_consumption_exponent string 
	Global_growth_rate string 
	Infra_allocation string 
	Content_creator_allocation string 
	Developer_allocation string 
	Validator_allocation string 
	Developer_min_deposit string 
	Developer_coin_return_interval_second int 
	Developer_coin_return_times int 
	Seconds_to_recover_bandwidth int 
	Capacity_usage_per_transaction string 
	Virtual_coin string
	Min_stake_in string 
	Voter_coin_return_interval_second int 
	Voter_coin_return_times int 
	Delegator_coin_return_interval_second int 
	Delegator_coin_return_times int 
	Content_censorship_decide_second int 
	Content_censorship_min_deposit string 
	Content_censorship_pass_ratio string 
	Content_censorship_pass_votes string 
	Change_param_decide_second int 
	Change_param_execution_second int 
	Change_param_min_deposit string 
	Change_param_pass_ratio string 
	Change_param_pass_votes string 
	Protocol_upgrade_decide_second int 
	Protocol_upgrade_min_deposit string 
	Protocol_upgrade_pass_ratio string 
    Protocol_upgrade_pass_votes string 
    Validator_min_withdraw string 
	Validator_min_voting_deposit string 
	Validator_min_commiting_deposit string 
	Validator_coin_return_second int 
	Validator_coin_return_times int 
	Penalty_miss_vote string 
	Penalty_miss_commit string 
    Penalty_byzantine string 
    Validator_list_size int 
	Absent_commit_limitation int 
	Seconds_to_recover_coin_day int 
	Minimum_balance string 
	Register_fee string 
	First_deposit_full_coin_day_limit string 
	Max_num_frozen_money int
	Report_or_upvote_interval_second int 
	Post_interval_sec int 
}

type balanceHistory struct {
    Details []struct {
        TO      string  `json:"to"`
        AMOUNT  struct {
			             Amount string `json:"amount"`
			           } `json:"amount"`
    } `json:"details"`
}

type accountBank struct {
	Saving      struct { Amount string `json:"amount"` } `json:"saving"`
	Coin_day    struct { Amount string `json:"amount"` } `json:"coin_day"`
	Frozen_ml []struct { Amount struct { Amount string `json:"amount"` } `json:"amount"`} `json:"frozen_money_list"`
	Num_of_tr   int `json:"number_of_transaction"`			 
	Num_of_rw   int `json:"number_of_reward"`	          	
}

type reward struct {
	Total_income      struct { Amount string `json:"amount"` } `json:"total_income"`
	Original_income      struct { Amount string `json:"amount"` } `json:"original_income"`
	Friction_income      struct { Amount string `json:"amount"` } `json:"friction_income"`
	Inflation_income      struct { Amount string `json:"amount"` } `json:"inflation_income"`
	Unclaim_reward      struct { Amount string `json:"amount"` } `json:"unclaim_reward"`	          	
}

type validators struct {
	Oncall_validators []string `json:"oncall_validators"`         	
}

type validatorInfo struct {
	Deposit struct { Amount string `json:"amount"` } `json:"deposit"`
	Absent_commit int `json:"absent_commit"`
	Produced_blocks int `json:"produced_blocks"`
	Address string `json:"address"`
}

type blocksList struct {
	Header struct {
		Height int `json:"height"`
		Time string `json:"time"`
		Num_txs int `json:"num_txs"`
		Chain_id string `json:"chain_id"`
		Total_txs int `json:"total_txs"` 
    } `json:"header"`
}

type blockStatus struct {
	Latest_block_height int `json:"latest_block_height"`
}

type ECVP struct {
	Consumption_time_adjust_base int `json:"consumption_time_adjust_base"`
	Consumption_time_adjust_offset int `json:"consumption_time_adjust_offset"`
	Num_of_consumption_on_author_offset int `json:"num_of_consumption_on_author_offset"`
	Total_amount_of_consumption_base int `json:"total_amount_of_consumption_base"`
	Total_amount_of_consumption_offset int `json:"total_amount_of_consumption_offset"`
	Amount_of_consumption_exponent string `json:"amount_of_consumption_exponent"`
}

type AllocationParam struct {
	Global_growth_rate string `json:"global_growth_rate"`
	Infra_allocation string `json:"infra_allocation"`
	Content_creator_allocation string `json:"content_creator_allocation"`
	Developer_allocation string `json:"developer_allocation"`
	Validator_allocation string `json:"validator_allocation"`
}

type DeveloperParam struct {
	Developer_min_deposit struct { Amount string `json:"amount"` } `json:"developer_min_deposit"`
	Developer_coin_return_interval_second int `json:"developer_coin_return_interval_second"`
	Developer_coin_return_times int `json:"developer_coin_return_times"`
}

type BandwidthParam struct {
	Seconds_to_recover_bandwidth int `json:"seconds_to_recover_bandwidth"`
	Capacity_usage_per_transaction struct { Amount string `json:"amount"` } `json:"capacity_usage_per_transaction"`
	Virtual_coin struct { Amount string `json:"amount"` } `json:"virtual_coin"`
}

type VoteParam struct {
	Min_stake_in struct { Amount string `json:"amount"` } `json:"min_stake_in"`
	Voter_coin_return_interval_second int `json:"voter_coin_return_interval_second"`
	Voter_coin_return_times int `json:"voter_coin_return_times"`
	Delegator_coin_return_interval_second int `json:"delegator_coin_return_interval_second"`
	Delegator_coin_return_times int `json:"delegator_coin_return_times"`
}

type ProposalParam struct {
	Content_censorship_decide_second int `json:"content_censorship_decide_second"`
	Content_censorship_min_deposit struct { Amount string `json:"amount"` } `json:"content_censorship_min_deposit"`
	Content_censorship_pass_ratio string `json:"content_censorship_pass_ratio"`
	Content_censorship_pass_votes struct { Amount string `json:"amount"` } `json:"content_censorship_pass_votes"`
	Change_param_decide_second int `json:"change_param_decide_second"`
	Change_param_execution_second int `json:"change_param_execution_second"`
	Change_param_min_deposit struct { Amount string `json:"amount"` } `json:"change_param_min_deposit"`
	Change_param_pass_ratio string `json:"change_param_pass_ratio"`
	Change_param_pass_votes struct { Amount string `json:"amount"` } `json:"change_param_pass_votes"`
	Protocol_upgrade_decide_second int `json:"protocol_upgrade_decide_second"`
	Protocol_upgrade_min_deposit struct { Amount string `json:"amount"` } `json:"protocol_upgrade_min_deposit"`
	Protocol_upgrade_pass_ratio string `json:"protocol_upgrade_pass_ratio"`
    Protocol_upgrade_pass_votes struct { Amount string `json:"amount"` } `json:"protocol_upgrade_pass_votes"`
}

type ValidatorParam struct {
	Validator_min_withdraw struct { Amount string `json:"amount"` } `json:"validator_min_withdraw"`
	Validator_min_voting_deposit struct { Amount string `json:"amount"` } `json:"validator_min_voting_deposit"`
	Validator_min_commiting_deposit struct { Amount string `json:"amount"` } `json:"validator_min_commiting_deposit"`
	Validator_coin_return_second int `json:"validator_coin_return_second"`
	Validator_coin_return_times int `json:"validator_coin_return_times"`
	Penalty_miss_vote struct { Amount string `json:"amount"` } `json:"penalty_miss_vote"`
	Penalty_miss_commit struct { Amount string `json:"amount"` } `json:"penalty_miss_commit"`
    Penalty_byzantine struct { Amount string `json:"amount"` } `json:"penalty_byzantine"`
    Validator_list_size int `json:"validator_list_size"`
	Absent_commit_limitation int `json:"absent_commit_limitation"` 
}

type CoinDayParam struct {
	Seconds_to_recover_coin_day int `json:"seconds_to_recover_coin_day"` 
}

type AccountParam struct {
	Minimum_balance struct { Amount string `json:"amount"` } `json:"minimum_balance"`
	Register_fee struct { Amount string `json:"amount"` } `json:"register_fee"`
	First_deposit_full_coin_day_limit struct { Amount string `json:"amount"` } `json:"first_deposit_full_coin_day_limit"`
	Max_num_frozen_money int `json:"max_num_frozen_money"` 
}

type PostParam struct {
	Report_or_upvote_interval_second int `json:"report_or_upvote_interval_second"` 
	Post_interval_sec int `json:"post_interval_sec"` 
}

func defaultHandler(w http.ResponseWriter, r *http.Request) {

    fp := path.Join("example/html/", "index.html")
    tmpl, err := template.ParseFiles(fp)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    
    if err := tmpl.Execute(w, nil); err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }    
}


func profileHandler(w http.ResponseWriter, r *http.Request) {
    if r.Form != nil {
		username = r.FormValue("username")
	}
	
    timeout := api.TimeoutOptions{QueryTimeout: 10 * time.Second, BroadcastTimeout: 15 * time.Second}
	api := api.NewLinoAPIFromArgs("lino-testnet", "http://fullnode.linovalidator.io:80", timeout)


    //Get balance History & parse JSON
    allBalanceHistory, _ := api.GetAllBalanceHistory(username)
	outputBalance, _ := json.Marshal(allBalanceHistory)
	
    var b_his = balanceHistory{}
    err := json.Unmarshal(outputBalance, &b_his)
    if err != nil {
        fmt.Println("Error!")
    }
    
    var amount [12]int64
    
    for i := 0; i < 12; i++ {
		amount_buf, err := strconv.ParseInt(b_his.Details[i].AMOUNT.Amount, 10, 64)
        if err != nil {
          panic(err)
        }
        amount[i] = amount_buf
	} 

	//Get accountBank & parse JSON
    accountBankInfo, _ := api.GetAccountBank(username)
	outputAccBank, _ := json.Marshal(accountBankInfo)
	
	var accBank = accountBank{}
    err1 := json.Unmarshal(outputAccBank, &accBank)
    if err1 != nil {
        fmt.Println("Error!")
    }
    saving_amount, _ := strconv.ParseInt(accBank.Saving.Amount, 10, 64)
	coin_day_amount, _ := strconv.ParseInt(accBank.Coin_day.Amount, 10, 64)
	num_of_tr := accBank.Num_of_tr
	num_of_rw := accBank.Num_of_rw

    //Get reward & parse JSON
    rewardInfo, _ := api.GetReward(username)
	outputReward, _ := json.Marshal(rewardInfo)
	fmt.Println(string(outputReward))
	
	var rew = reward{}
    err2 := json.Unmarshal(outputReward, &rew)
    if err2 != nil {
        fmt.Println("Error!")
    }
    total_income, _ := strconv.ParseInt(rew.Total_income.Amount, 10, 64)
	original_income, _ := strconv.ParseInt(rew.Original_income.Amount, 10, 64)
	friction_income, _ := strconv.ParseInt(rew.Friction_income.Amount, 10, 64)
	inflation_income, _ := strconv.ParseInt(rew.Inflation_income.Amount, 10, 64)
	unclaim_reward, _ := strconv.ParseInt(rew.Unclaim_reward.Amount, 10, 64)
	
	//Get validators & validatorInfo
	validatorsList, _ := api.GetAllValidators()
	outputValidators, _ := json.Marshal(validatorsList)
	fmt.Println(string(outputValidators))
	
	var valList = validators{}
    err3 := json.Unmarshal(outputValidators, &valList)
    if err3 != nil {
        fmt.Println("Error!")
    }
  
    count := 1
    for i, valNum := range valList.Oncall_validators {
      count = i 
      fmt.Println(valNum)
    }
    valListStr := make([]string, count)
    
    val_address := make([]string, count)
    val_deposit := make([]string, count)
    val_absent_commit := make([]int, count)
    val_produced_blocks := make([]int, count)
    
    for i := 0; i < count; i++ {
		valListStr[i] = valList.Oncall_validators[i]
		
		validator, _ := api.GetValidator(valList.Oncall_validators[i])
	    outputValidator, _ := json.Marshal(validator)
		
		var valInfo = validatorInfo{}
        err4 := json.Unmarshal(outputValidator, &valInfo)
		if err4 != nil {
            fmt.Println("Error!")
        }
        
        val_deposit[i] = valInfo.Deposit.Amount
        val_absent_commit[i] = valInfo.Absent_commit
        val_produced_blocks[i] = valInfo.Produced_blocks
        val_address[i] = valInfo.Address	
	}
	
	//Get blocks
	latestBlock, _ := api.GetBlockStatus()
	outputLatestBlock, _ := json.Marshal(latestBlock)
	
	var lBlock = blockStatus{}
    err4 := json.Unmarshal(outputLatestBlock, &lBlock)
    if err4 != nil {
        fmt.Println("Error!")
    }
   
    var latestBlockHeight [10]int64
    var latestBlockTime [10]string
    var latestBlockNum_txs [10]int64
    var latestBlockTot_txs [10]int64
    var latestBlockChain_id[10]string
    
    latestBlockHeight[0] = int64(lBlock.Latest_block_height)  
    n := 1
    cnt := 1
    
    blockInfo, _ := api.GetBlock(int64(lBlock.Latest_block_height))
	outputBlockInfo, _ := json.Marshal(blockInfo)
	
    var bInfo = blocksList{}
    err5 := json.Unmarshal(outputBlockInfo, &bInfo)
    if err5 != nil {
        fmt.Println("Error!")
    }
    
    latestBlockTime[0] = bInfo.Header.Time
    latestBlockNum_txs[0] = int64(bInfo.Header.Num_txs)
    latestBlockChain_id[0] = bInfo.Header.Chain_id
    latestBlockTot_txs[0] = int64(bInfo.Header.Total_txs)
    
    //Get another blocks info
    for i := 10; i > n; i-- {
		latestBlockHeight[cnt] = int64(lBlock.Latest_block_height) - int64(cnt)
		fmt.Println(latestBlockHeight[cnt])
		blockInfo, _ := api.GetBlock(int64(lBlock.Latest_block_height) - int64(cnt))
	    outputBlockInfo, _ := json.Marshal(blockInfo)
	    
	    var bInfo = blocksList{}
        err6 := json.Unmarshal(outputBlockInfo, &bInfo)
        if err6 != nil {
            fmt.Println("Error!")
        }
		
		latestBlockTime[cnt] = bInfo.Header.Time
        latestBlockNum_txs[cnt] = int64(bInfo.Header.Num_txs)
        latestBlockChain_id[cnt] = bInfo.Header.Chain_id
        latestBlockTot_txs[cnt] = int64(bInfo.Header.Total_txs)
        
        cnt = cnt + 1      
	} 
    	
    //Response for HTML template
    data := ViewData{
		Username : username,
		Amount : amount,
		Saving_amount : saving_amount,
		Coin_day_amount : coin_day_amount,
		Num_of_tr : num_of_tr,
		Num_of_rw : num_of_rw,
		Total_income  : total_income,
	    Original_income : original_income,
	    Friction_income : friction_income,
	    Inflation_income : inflation_income,  
	    Unclaim_reward : unclaim_reward,
	    ValidatorsList : valListStr,
	    Val_deposit : val_deposit,
	    Val_absent_commit : val_absent_commit, 
	    Val_produced_blocks : val_produced_blocks,
	    Val_address : val_address,
	    LatestBlockTime : latestBlockTime,
	    LatestBlockNum_txs : latestBlockNum_txs,
	    LatestBlockChain_id : latestBlockChain_id,
	    LatestBlockTot_txs : latestBlockTot_txs,
	    LatestBlockHeight : latestBlockHeight,
	}
    
    fp := path.Join("example/html/", "dashboard.html")
    tmpl, err := template.ParseFiles(fp)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    if err := tmpl.Execute(w, data); err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }
    
}


func paramsHandler(w http.ResponseWriter, r *http.Request) {
	timeout := api.TimeoutOptions{QueryTimeout: 10 * time.Second, BroadcastTimeout: 15 * time.Second}
	api := api.NewLinoAPIFromArgs("lino-testnet", "http://fullnode.linovalidator.io:80", timeout)


    //Get Evaluate Of Content Value Param
    evContentValueParam, _ :=  api.GetEvaluateOfContentValueParam()
	outputECVP, _ := json.Marshal(evContentValueParam)
	
    var ecvp = ECVP{}
    err := json.Unmarshal(outputECVP, &ecvp)
    if err != nil {
        fmt.Println("Error!")
    }
    
	consumption_time_adjust_base := ecvp.Consumption_time_adjust_base 
	consumption_time_adjust_offset := ecvp.Consumption_time_adjust_offset 
	num_of_consumption_on_author_offset := ecvp.Num_of_consumption_on_author_offset 
	total_amount_of_consumption_base := ecvp.Total_amount_of_consumption_base 
	total_amount_of_consumption_offset := ecvp.Total_amount_of_consumption_offset 
	amount_of_consumption_exponent := ecvp.Amount_of_consumption_exponent 

    //Get Allocation params
	allocationParam, _ := api.GetGlobalAllocationParam()
	outputAllocationParam, _ := json.Marshal(allocationParam)
	
	var allocParam = AllocationParam{}
    err1 := json.Unmarshal(outputAllocationParam, &allocParam)
    if err1 != nil {
        fmt.Println("Error!")
    }
	
	global_growth_rate := allocParam.Global_growth_rate
	infra_allocation := allocParam.Infra_allocation
	content_creator_allocation := allocParam.Content_creator_allocation
	developer_allocation := allocParam.Developer_allocation
	validator_allocation := allocParam.Validator_allocation
	
	//Get Developer params
	developerParam, _ := api.GetDeveloperParam()
	outputDeveloperParam, _ := json.Marshal(developerParam)

    var devParam = DeveloperParam{}
    err2 := json.Unmarshal(outputDeveloperParam, &devParam)
    if err2 != nil {
        fmt.Println("Error!")
    }
    
    developer_min_deposit := devParam.Developer_min_deposit.Amount
	developer_coin_return_interval_second := devParam.Developer_coin_return_interval_second
	developer_coin_return_times := devParam.Developer_coin_return_times
	
    //Get Bandwidth params
	bandwidthParam, _ := api.GetBandwidthParam()
	outputBandwidthParam, _ := json.Marshal(bandwidthParam)

    var bandParam = BandwidthParam{}
    err3 := json.Unmarshal(outputBandwidthParam, &bandParam)
    if err3 != nil {
        fmt.Println("Error!")
    }

    seconds_to_recover_bandwidth := bandParam.Seconds_to_recover_bandwidth
    capacity_usage_per_transaction := bandParam.Capacity_usage_per_transaction.Amount
    virtual_coin := bandParam.Virtual_coin.Amount

    //Get Vote params
	voteParam, _ := api.GetVoteParam()
	outputVoteParam, _ := json.Marshal(voteParam)
    
    var vParam = VoteParam{}
    err4 := json.Unmarshal(outputVoteParam, &vParam)
    if err4 != nil {
        fmt.Println("Error!")
    }
    
    min_stake_in := vParam.Min_stake_in.Amount
    voter_coin_return_interval_second := vParam.Voter_coin_return_interval_second
    voter_coin_return_times := vParam.Voter_coin_return_times
    delegator_coin_return_interval_second := vParam.Delegator_coin_return_interval_second
    delegator_coin_return_times := vParam.Delegator_coin_return_times

    //Get Proposal params
	proposalParam, _ := api.GetProposalParam()
	outputProposalParam, _ := json.Marshal(proposalParam)

    var pParam = ProposalParam{}
    err5 := json.Unmarshal(outputProposalParam, &pParam)
    if err5 != nil {
        fmt.Println("Error!")
    }
    
    content_censorship_decide_second := pParam.Content_censorship_decide_second
    content_censorship_min_deposit := pParam.Content_censorship_min_deposit.Amount
    content_censorship_pass_ratio := pParam.Content_censorship_pass_ratio
    content_censorship_pass_votes := pParam.Content_censorship_pass_votes.Amount
    change_param_decide_second := pParam.Change_param_decide_second
    change_param_execution_second := pParam.Change_param_execution_second
    change_param_min_deposit := pParam.Change_param_min_deposit.Amount
    change_param_pass_ratio := pParam.Change_param_pass_ratio
    change_param_pass_votes := pParam.Change_param_pass_votes.Amount
    protocol_upgrade_decide_second := pParam.Protocol_upgrade_decide_second
    protocol_upgrade_min_deposit := pParam.Protocol_upgrade_min_deposit.Amount
    protocol_upgrade_pass_ratio := pParam.Protocol_upgrade_pass_ratio
    protocol_upgrade_pass_votes := pParam.Protocol_upgrade_pass_votes.Amount

    //Get Validator params
	validatorParam, _ := api.GetValidatorParam()
	outputValidatorParam, _ := json.Marshal(validatorParam)

    var valParam = ValidatorParam{}
    err6 := json.Unmarshal(outputValidatorParam, &valParam)
    if err6 != nil {
        fmt.Println("Error!")
    }
    
    validator_min_withdraw := valParam.Validator_min_withdraw.Amount
    validator_min_voting_deposit := valParam.Validator_min_voting_deposit.Amount
    validator_min_commiting_deposit := valParam.Validator_min_commiting_deposit.Amount
    validator_coin_return_second := valParam.Validator_coin_return_second
    validator_coin_return_times := valParam.Validator_coin_return_times
    penalty_miss_vote := valParam.Penalty_miss_vote.Amount
    penalty_miss_commit := valParam.Penalty_miss_commit.Amount
    penalty_byzantine := valParam.Penalty_byzantine.Amount
    validator_list_size := valParam.Validator_list_size
	absent_commit_limitation := valParam.Absent_commit_limitation

    //Get Coin day params
	coinDayParam, _ := api.GetCoinDayParam()
	outputCoinDayParam, _ := json.Marshal(coinDayParam)

    var cdParam = CoinDayParam{}
    err7 := json.Unmarshal(outputCoinDayParam, &cdParam)
    if err7 != nil {
        fmt.Println("Error!")
    }
    
    seconds_to_recover_coin_day := cdParam.Seconds_to_recover_coin_day

    //Get Account params
	accountParam, _ := api.GetAccountParam()
	outputAccountParam, _ := json.Marshal(accountParam)
	
	var accParam = AccountParam{}
    err8 := json.Unmarshal(outputAccountParam, &accParam)
    if err8 != nil {
        fmt.Println("Error!")
    }
    
    minimum_balance := accParam.Minimum_balance.Amount
    register_fee := accParam.Register_fee.Amount
	first_deposit_full_coin_day_limit := accParam.First_deposit_full_coin_day_limit.Amount
    max_num_frozen_money := accParam.Max_num_frozen_money
    
    //Get Post params
	postParam, _ := api.GetPostParam()
	outputPostParam, _ := json.Marshal(postParam)

	var pstParam = PostParam{}
    err9 := json.Unmarshal(outputPostParam, &pstParam)
    if err9 != nil {
        fmt.Println("Error!")
    }
    
    report_or_upvote_interval_second := pstParam.Report_or_upvote_interval_second
    post_interval_sec := pstParam.Post_interval_sec
      	
    //Response for HTML template
    data := ParamData{
		Username : username,
        Consumption_time_adjust_base : consumption_time_adjust_base,
	    Consumption_time_adjust_offset : consumption_time_adjust_offset,
	    Num_of_consumption_on_author_offset : num_of_consumption_on_author_offset, 
	    Total_amount_of_consumption_base : total_amount_of_consumption_base,
	    Total_amount_of_consumption_offset : total_amount_of_consumption_offset,
	    Amount_of_consumption_exponent : amount_of_consumption_exponent,
	    Global_growth_rate : global_growth_rate,
	    Infra_allocation : infra_allocation,
	    Content_creator_allocation : content_creator_allocation,
    	Developer_allocation : developer_allocation,
	    Validator_allocation : validator_allocation,
	    Developer_min_deposit : developer_min_deposit,
	    Developer_coin_return_interval_second : developer_coin_return_interval_second,
	    Developer_coin_return_times : developer_coin_return_times, 
	    Seconds_to_recover_bandwidth : seconds_to_recover_bandwidth, 
	    Capacity_usage_per_transaction : capacity_usage_per_transaction, 
	    Virtual_coin : virtual_coin,
	    Min_stake_in : min_stake_in, 
	    Voter_coin_return_interval_second : voter_coin_return_interval_second,
	    Voter_coin_return_times : voter_coin_return_times,
	    Delegator_coin_return_interval_second : delegator_coin_return_interval_second, 
	    Delegator_coin_return_times : delegator_coin_return_times,
	    Content_censorship_decide_second : content_censorship_decide_second,
    	Content_censorship_min_deposit : content_censorship_min_deposit,
    	Content_censorship_pass_ratio : content_censorship_pass_ratio, 
	    Content_censorship_pass_votes : content_censorship_pass_votes,
	    Change_param_decide_second : change_param_decide_second, 
	    Change_param_execution_second : change_param_execution_second,
    	Change_param_min_deposit : change_param_min_deposit, 
    	Change_param_pass_ratio : change_param_pass_ratio,
    	Change_param_pass_votes : change_param_pass_votes, 
	    Protocol_upgrade_decide_second : protocol_upgrade_decide_second,
    	Protocol_upgrade_min_deposit : protocol_upgrade_min_deposit,
	    Protocol_upgrade_pass_ratio : protocol_upgrade_pass_ratio,
        Protocol_upgrade_pass_votes : protocol_upgrade_pass_votes,
        Validator_min_withdraw : validator_min_withdraw,
	    Validator_min_voting_deposit : validator_min_voting_deposit,
	    Validator_min_commiting_deposit : validator_min_commiting_deposit,
	    Validator_coin_return_second : validator_coin_return_second,
    	Validator_coin_return_times : validator_coin_return_times,
	    Penalty_miss_vote : penalty_miss_vote,
	    Penalty_miss_commit : penalty_miss_commit,
        Penalty_byzantine : penalty_byzantine,
        Validator_list_size : validator_list_size,
	    Absent_commit_limitation : absent_commit_limitation,
	    Seconds_to_recover_coin_day : seconds_to_recover_coin_day,
		Minimum_balance : minimum_balance,
	    Register_fee : register_fee,
	    First_deposit_full_coin_day_limit : first_deposit_full_coin_day_limit,
	    Max_num_frozen_money : max_num_frozen_money,
	    Report_or_upvote_interval_second : report_or_upvote_interval_second, 
	    Post_interval_sec : post_interval_sec,
	}
    
    fp := path.Join("example/html/", "params.html")
    tmpl, err := template.ParseFiles(fp)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    if err := tmpl.Execute(w, data); err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }
    
}

func main() {
	http.HandleFunc("/", defaultHandler)
	http.HandleFunc("/profile", profileHandler)
    http.HandleFunc("/params", paramsHandler)
	
       
    fs := http.FileServer(http.Dir("./example/html/static"))
    http.Handle("/static/", http.StripPrefix("/static/", fs))
    
    http.ListenAndServe(":8080", nil)
}